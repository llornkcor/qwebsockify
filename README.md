QWebSockify is a tool that presents a WebSocket interface that targets a tcp socket.
It sends communications between the two.

WebSocket <=> QSocket

A simple Qt version of websockify used in Emscripten.
