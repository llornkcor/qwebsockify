#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtWebSockets/QWebSocketServer>
#include <QtWebSockets/QWebSocket>
#include <QAbstractSocket>
#include <QtNetwork>
#include <QWebSocketProtocol>
#include <QTimer>
#include <QString>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    webSocketServer(nullptr),
    webSocketClient(nullptr),
    tcpSocket(nullptr),
    ui(new Ui::MainWindow),
    binaryRequested(false),
    useTcp(true)
{
    ui->setupUi(this);

    QList<QHostAddress> list = QNetworkInterface::allAddresses();
    for (int nIter=0; nIter<list.count(); nIter++) {
        if (!list[nIter].isLoopback())
            if (list[nIter].protocol() == QAbstractSocket::IPv4Protocol ) {
                ui->serverHostLineEdit->setText(list[nIter].toString());
                break;
            }
    }
}

MainWindow::~MainWindow()
{
    stopTargetSocket();
    delete ui;
}

void MainWindow::on_startButton_clicked()
{
    if (webSocketServer && webSocketServer->isListening())
        return;

    if (!webSocketsAddress.setAddress(ui->webSocketAddressLineEdit->text())) {
        //error
        ui->textEdit->setPlainText("Error setting websocket address\n");
        return;
    }

    if (!targetServerAddress.setAddress(ui->serverHostLineEdit->text())) {
        //error
        ui->textEdit->setPlainText("Error setting target server address\n");
        return;
    }

    websocketPort = ui->websocketSpinBox->value();
    targetServerPort = ui->serverHostSpinBox->value();

    initWebsockets();
    initTargetSocket();
}

void MainWindow::on_stopButton_clicked()
{
    qDebug() << Q_FUNC_INFO << __LINE__;
    stopTargetSocket();
    stopWebSocket();
}

void MainWindow::stopWebSocket()
{
    qDebug() << Q_FUNC_INFO;

    if (webSocketServer) {
        if (webSocketServer->isListening())
            webSocketServer->close();

        if (webSocketClient && webSocketClient->isValid())
            webSocketClient->close();
    }
}

void MainWindow::stopTargetSocket()
{
    qDebug() << Q_FUNC_INFO << __LINE__;

    if (useTcp) {
        if (tcpSocket && tcpSocket->isValid() && tcpSocket->state() != QAbstractSocket::UnconnectedState) {
            tcpSocket->flush();
            tcpSocket->disconnectFromHost();
            tcpSocket->close();
        }
    }
}

void MainWindow::initWebsockets()
{
    qDebug() << Q_FUNC_INFO << __LINE__;
    if (!webSocketServer || webSocketServer->isListening()) {

        on_stopButton_clicked();

        webSocketServer = new QWebSocketServer("QWebSockify", QWebSocketServer::NonSecureMode, this);

        connect(webSocketServer, &QWebSocketServer::newConnection, this, &MainWindow::newConnection);
        connect(webSocketServer, &QWebSocketServer::closed, this, &MainWindow::websocketServerClosed);
        connect(webSocketServer, &QWebSocketServer::serverError, this, &MainWindow::serverError);

        if (webSocketServer->listen(webSocketsAddress, websocketPort)) {
            QString msg = QString("WebSocket waiting for connection on %1 port %2\n").arg(webSocketsAddress.toString()).arg(websocketPort);
            ui->textEdit->insertPlainText(msg);
        }
    }

}

void MainWindow::initTargetSocket()
{
    qDebug() << Q_FUNC_INFO << __LINE__;
    if (tcpSocket) {
        delete tcpSocket;
        tcpSocket = nullptr;
    }


    if (useTcp) {
        if (!tcpSocket) {
            qDebug() << Q_FUNC_INFO << "new tcp socket";
            tcpSocket = new QTcpSocket(this);
        }
        if (tcpSocket) {
            connect(tcpSocket, &QTcpSocket::connected,
                    this, &MainWindow::serverConnected);

            connect(tcpSocket, &QTcpSocket::disconnected,
                    this, &MainWindow::serverDisconnected);

            connect(tcpSocket, &QTcpSocket::errorOccurred,
                    [=](QAbstractSocket::SocketError /*socketError*/){
                ui->textEdit->insertPlainText(tcpSocket->errorString() + "\n");
            });

            connect(tcpSocket, &QTcpSocket::readyRead,
                    this, &MainWindow::readServerMessage);
            in.setDevice(tcpSocket);
        }
    } else {
        // udp target
        udpSocket = new QUdpSocket(this);
        udpSocket->bind(targetServerPort, QUdpSocket::ShareAddress); // ? for receiving

        connect(udpSocket, &QUdpSocket::connected,
                this, &MainWindow::serverConnected);

        connect(udpSocket, &QUdpSocket::disconnected,
                this, &MainWindow::serverDisconnected);

        connect(udpSocket, &QUdpSocket::errorOccurred,
                [=](QAbstractSocket::SocketError /*socketError*/){
            ui->textEdit->insertPlainText(udpSocket->errorString() + "\n");
        });

        connect(udpSocket, &QUdpSocket::readyRead,
                this, &MainWindow::readServerMessage);
        in.setDevice(udpSocket);
    }

    in.setVersion(QDataStream::Qt_4_0);
}

void MainWindow::newConnection()
{
    qDebug() << Q_FUNC_INFO << __LINE__;
    ui->textEdit->insertPlainText("new WebSocket client connected\n");

    if (useTcp)
        connectToServer();

    if (webSocketClient)
        delete webSocketClient;

    webSocketClient = webSocketServer->nextPendingConnection();

    connect(webSocketClient, QOverload<QAbstractSocket::SocketError>::of(&QWebSocket::error),
            this, &MainWindow::socketError);

    connect(webSocketClient, &QWebSocket::textMessageReceived, this,
            &MainWindow::textMessageReceived);
    connect(webSocketClient, &QWebSocket::binaryMessageReceived,
            this, &MainWindow::binaryReceived);

    connect(webSocketClient, &QWebSocket::disconnected,
            this, &MainWindow::websocketClientDisconnected);

    if (!webSocketClient->error())
        websocketClientConnected();
}

void MainWindow::socketError(QAbstractSocket::SocketError error)
{
    qDebug() << "WebSocket error" << error;
    // tcpSocket
}

void MainWindow::textMessageReceived(const QString &msg)
{
    qDebug() << Q_FUNC_INFO << __LINE__;
    ui->textEdit->insertPlainText("from websocket: "+msg+"\n");

    if (tcpSocket && (!tcpSocket->isOpen() && tcpSocket->state() != QAbstractSocket::ConnectedState)) {
        connectToServer();
    }

    if (useTcp) {
        if (tcpSocket && !tcpSocket->isOpen())
            return;
        tcpSocket->write(msg.toLocal8Bit().data());
        tcpSocket->flush();
    } else {
        udpSocket->writeDatagram(msg.toLocal8Bit().data(), targetServerAddress, targetServerPort);
    }

}

void MainWindow::binaryReceived(const QByteArray &bMsg)
{
    qDebug() << Q_FUNC_INFO << __LINE__;
    ui->textEdit->insertPlainText("from websocket: "+bMsg+"\n");

    if (useTcp) {
        if (tcpSocket && !tcpSocket->isOpen())
            return;
        tcpSocket->write(bMsg);
        tcpSocket->flush();

    } else {
        udpSocket->writeDatagram(bMsg, targetServerAddress, targetServerPort);
    }

    if (binaryRequested)
        webSocketClient->sendBinaryMessage(bMsg);
}

void MainWindow::connectToServer()
{
    qDebug() << Q_FUNC_INFO << tcpSocket ;
    if (!tcpSocket) {
        initTargetSocket();
    }
    qDebug() << Q_FUNC_INFO << "is open" << tcpSocket->isOpen()
             << "state" << tcpSocket->state();

    if (useTcp) {
        if (tcpSocket && tcpSocket->state() == QAbstractSocket::UnconnectedState)
            tcpSocket->connectToHost(targetServerAddress, targetServerPort, QIODevice::ReadWrite);
    }
}

void MainWindow::serverError(QWebSocketProtocol::CloseCode closeCode)
{
    qDebug() << Q_FUNC_INFO << closeCode;
    ui->textEdit->insertPlainText(webSocketServer->errorString() + "\n");
}

void MainWindow::websocketServerClosed()
{
    qDebug() << Q_FUNC_INFO;
    ui->textEdit->insertPlainText("WebSocket server closed\n");
    delete webSocketServer;
    webSocketServer = 0;
}

void MainWindow::websocketClientDisconnected()
{
    qDebug() << Q_FUNC_INFO;
    ui->textEdit->insertPlainText("WebSocket client disconnected\n");
}

void MainWindow::websocketClientConnected()
{
    qDebug() << Q_FUNC_INFO;
    ui->textEdit->insertPlainText("WebSocket client connected\n");
}

void MainWindow::serverConnected()
{
    qDebug() << Q_FUNC_INFO << __LINE__;
    ui->textEdit->insertPlainText("Server connected\n");
}

void MainWindow::serverDisconnected()
{
    ui->textEdit->insertPlainText("Server disconnected\n");
}

void MainWindow::readServerMessage()
{
    in.startTransaction();

    QString nextFortune;
    in >> nextFortune;

    if (!in.commitTransaction())
        return;

    qDebug() << Q_FUNC_INFO << nextFortune;

    ui->textEdit->insertPlainText("Message received from target server:\n");
    //qDebug() << Q_FUNC_INFO << tcpSocket <<  tcpSocket->bytesAvailable();
    //    if (tcpSocket && tcpSocket->bytesAvailable()) {
    //QByteArray msg = tcpSocket->readAll();
    qDebug() << Q_FUNC_INFO << nextFortune.size() << nextFortune;
    ui->textEdit->insertHtml("<b>"+nextFortune + "</b><br>");

    if (webSocketClient && webSocketClient->isValid()) {
        webSocketClient->sendTextMessage(nextFortune);
        nextFortune.clear();
    }
}

void MainWindow::on_clearButton_clicked()
{
    ui->textEdit->clear();
}


void MainWindow::on_radioButton_clicked(bool checked)
{
    qDebug() << Q_FUNC_INFO;
    binaryRequested = checked;
}


void MainWindow::on_tcpUdpButton_toggled(bool checked)
{
    useTcp = !checked;
    if (!checked) {
        ui->tcpUdpButton->setText("Tcp");
    }   else {
        ui->tcpUdpButton->setText("Udp");
    }
    qDebug() << Q_FUNC_INFO << "useTcp" << useTcp;
}
