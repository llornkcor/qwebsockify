#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWebSocketServer>
#include <QWebSocket>
#include <QWebSocketProtocol>
#include <QTcpSocket>
#include <QUdpSocket>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QWebSocketServer *webSocketServer;

    QWebSocket *webSocketClient;
    QHostAddress webSocketsAddress;
    quint16 websocketPort;

    QHostAddress targetServerAddress;
    quint16 targetServerPort;
    QTcpSocket *tcpSocket;
    QUdpSocket *udpSocket;

    void initWebsockets();
    void initTargetSocket();

    void connectToServer();
    QDataStream in;

private slots:
    void on_startButton_clicked();

    void on_stopButton_clicked();

    void on_clearButton_clicked();

    void websocketServerClosed();
    void newConnection();
    void socketError(QAbstractSocket::SocketError error);
    void textMessageReceived(const QString &msg);
    void binaryReceived(const QByteArray &bMsg);

    void serverError(QWebSocketProtocol::CloseCode closeCode);

    void websocketClientDisconnected();
    void websocketClientConnected();

    void serverConnected();
    void serverDisconnected();

    void readServerMessage();

    void stopTargetSocket();
    void stopWebSocket();

    void on_radioButton_clicked(bool checked);
    void on_tcpUdpButton_toggled(bool checked);

private:
    Ui::MainWindow *ui;
    bool binaryRequested;
    bool useTcp;
};

#endif // MAINWINDOW_H
